#include "cmdprocess.h"

cmdProcess::cmdProcess( uSocket *s)
{
    sock = s;
}
void cmdProcess::doProcessing(char * cmd){
    if (strstr(cmd,"echo")){
        //sock->sendBytes("\n",1);
        sock->sendBytes(cmd+5,strlen(cmd)-5);
        sock->sendBytes("\0",1);
        return;
    }
    if (strstr(cmd,"time")){
        sock->sendBytes("\n",1);
        tm tp;
        long int s_time = time (NULL);
        char buf[26]="";
        localtime_r (&s_time, &tp);
        asctime_r (&tp,buf);
        sock->sendBytes(buf,strlen(buf)+1);
        return;
    }
    if (strstr(cmd,"bye")){
        sock->disconnectSock();
        return;
    }
    if (strstr(cmd,"get")){
        getF(cmd);
        return;
    }
    if (strstr(cmd,"put")){
        putF(cmd);
        return;
    }
    sock->sendBytes("unknown command",16);

}

void cmdProcess::getF(char * cmd){
    FILE * f = fopen(cmd+4,"rb");
    if (f == NULL){
        sock->sendBytes(emptyFileSize,14);
        return;
    }
    fseek(f, 0L, SEEK_END);
    long sz = ftell(f);
    char asciiSize[14];
    char buf[BLOCK];
    char * tmp =(char*) to_string(sz).c_str();
    strcpy(asciiSize,tmp);
    sock->sendBytes(asciiSize,14);
    sock->recvBytes((char*)&filePos,sizeof(long long));
    sz-=filePos;
    int bs = BLOCK;
    rewind(f);
    fseek(f,filePos,SEEK_CUR);
    unsigned s = 0;
    char c = 'A';
    while (sz>0) {
        if (sz < BLOCK){
            bs = sz;
        }
        fread(buf,bs,1,f);
        if(sock->sendBytes(buf,bs) == SOCKET_ERROR){
            cout<<"DISCONNECTED"<<endl;
            sock->disconnectSock();
            return;
        }
        sz-=bs;
        s++;
        if (!(s%1000)){
        cout << sz << endl; //прогресс
        if (!(s%2000)) sock->sendOOB(c);
        c++;
        }
    }
    fclose(f);
    filePos=0;
}

void cmdProcess::putF(char * cmd){
    char response[14];
    sock->recvBytes(response,14);
    long sz= atoi(response);
    if (!sz){
        cout << "file not found or empty" << endl;
        return;
    }
    if(filePos && strcmp(lastUploadedFile,response)){
        filePos = 0;
    }
    sock->sendBytes((char*)&filePos,sizeof(long long)); //с какого байта хотим продолжить?
    FILE * f;
    if (filePos){
     f = fopen(cmd+4,"rb+"); //докачиваем
    } else {
     f = fopen(cmd+4,"wb");
    }
    fseek(f,filePos,SEEK_SET);
    char  buf[BLOCK];
    int bs = BLOCK;
    sz-=filePos;
    cout << "sz "<< sz <<endl;
    unsigned s = 0;
    while (sz>0) {
        if (sz < BLOCK){
            bs = sz;
        }
          int status = sock->recvBytes(buf,bs);
        if(			status ==  SOCKET_ERROR ){
            cout<<"DISCONNECTED"<<endl;
            filePos = ftell(f);   //какой байт в файл записан последним
            strcpy(lastUploadedFile,response); //какой файл был не докачан?
            fclose(f);
            sock->disconnectSock();
            return;
        }
        fwrite(buf,status,1,f);
        sz-=status;
        s++;
        if (!(s%1000)){
        cout << sz << endl; //прогресс
        }
    }
    fclose(f);
    filePos = 0;
}
