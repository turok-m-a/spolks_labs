#include "usocket.h"
#ifdef linux
char oobByte;
volatile  char oobReceived = 0;
int * sockPtr;
void sig_urg(int signo)
{
     printf("SIGURG received\n");
     recv(*sockPtr, &oobByte, 1, MSG_OOB);
     oobReceived = 1;
 }
#endif
uSocket::uSocket()
{
#ifdef WIN32
    WSADATA lp;
    WSAStartup(MAKEWORD( 2, 2 ),&lp);
#endif
    s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
#ifdef WIN32
    timeout = CLIENT_TIMEOUT;
#elif linux
    timeout.tv_sec = CLIENT_TIMEOUT/1000;
    timeout.tv_usec = 0;
#endif
    setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
    SOCKADDR_IN sin;
        sin.sin_family = PF_INET;
        sin.sin_port = htons(10001);
        sin.sin_addr.s_addr = INADDR_ANY;
    int retVal = bind(s, (LPSOCKADDR)&sin, sizeof(sin));
    if(retVal == SOCKET_ERROR)
            {
                cout<<"Unable to bind\n";
                #ifdef WIN32
                WSACleanup();
                #endif
                return;
            }
    //Пытаемся начать слушать сокет
       retVal = listen(s, SOMAXCONN);
       if(retVal == SOCKET_ERROR)
       {
           cout<<"Unable to listen\n";
           #ifdef WIN32
           WSACleanup();
           #endif
           return;
       }

}
void uSocket::acceptConn(){
        clientSock = accept(s, NULL, NULL);

        if(clientSock == INVALID_SOCKET)
        {
            printf("Unable to accept\n");
            #ifdef WIN32
            WSACleanup();
            #endif
            return;
        }
        cout<<"Got the request from client\n";
#ifdef linux
     setsockopt(clientSock,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
     timeout.tv_sec = 10;
     setsockopt(clientSock,SOL_SOCKET,SO_SNDTIMEO,(const char*)&timeout,sizeof(timeout));
     signal(SIGURG, sig_urg);
     sockPtr = &clientSock;
#elif WIN32
     setsockopt(clientSock,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
#endif
}

int uSocket::recvBytes(char * buf,int bytes){
#ifdef WIN32
    timeout = 10000;
#elif linux
    timeout.tv_sec=10;
#endif


#ifdef WIN32
    setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
    setsockopt(clientSock,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
#elif linux
    setsockopt(clientSock,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
#endif

       int ret = recv(clientSock,buf,bytes,0);
#ifdef WIN32
    timeout = CLIENT_TIMEOUT;
#elif linux
    timeout.tv_sec=CLIENT_TIMEOUT/1000;
#endif

    #ifdef WIN32
    setsockopt(clientSock,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
    setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
   #elif linux
     setsockopt(clientSock,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
   #endif
    return ret;
}
void uSocket::recvCmd(char * buf){

    buf--;
    do{
        buf++;
        if (recv(clientSock,buf,1,0) < 0) {
            cout << "client timeout" <<endl;
            uSleep(2000);
            *buf = '\0';
            return disconnectSock();
        }
        cout << *buf << endl;       //пускай будет до версии 1.0
    } while((*buf)!='\0');

}
int uSocket::sendBytes(char * buf,int bytes){
    return send(clientSock,buf,bytes,0);
}

int uSocket::sendOOB(char a)
{
    return send(clientSock,&a,1,MSG_OOB);
}

bool uSocket::recbOOB(char &a)
{
#ifdef linux
    if(!oobReceived) return false;
    oobReceived = 0;
    a = oobByte;
    return true;
#else
    unsigned long nonBlock = 1;
    unsigned long Block = 0;
    ioctlsocket(clientSock,FIONBIO,&nonBlock);
    if (recv(clientSock,&a,1,MSG_OOB) == WSAEWOULDBLOCK){
        ioctlsocket(clientSock,FIONBIO,&Block);
        return false;
    }
    ioctlsocket(clientSock,FIONBIO,&Block);
    return true;
#endif
}
void uSocket::disconnectSock(){
#ifdef WIN32
    closesocket(clientSock);
#elif linux
    close(clientSock);
#endif
    clientSock = accept(s, NULL, NULL);
#ifdef WIN32
    timeout = CLIENT_TIMEOUT;
    setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
#elif linux
    timeout.tv_sec = CLIENT_TIMEOUT/1000;
    setsockopt(clientSock,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
    timeout.tv_sec = 10;
    setsockopt(clientSock,SOL_SOCKET,SO_SNDTIMEO,(const char*)&timeout,sizeof(timeout));
#endif
}
uSocket::~uSocket(){
#ifdef WIN32
    closesocket(clientSock);
    closesocket(s);
    WSACleanup();
#elif linux
    close(clientSock);
    close(s);
#endif
}
void uSocket::uSleep(int i){
#ifdef WIN32
    Sleep(i);
#elif linux
    sleep(i/1000);
#endif
}
