#ifndef CMDPROCESS_H
#define CMDPROCESS_H
#include <usocket.h>
#include <time.h>
#include <iostream>
#define BLOCK 10000
class cmdProcess
{
private:
    uSocket * sock;
    long long filePos = 0;
    void getF(char *);
    void putF(char *);
    char lastUploadedFile[256];
    char emptyFileSize[14] = "0";
public:
    cmdProcess(uSocket *);
    void doProcessing(char *);
};

#endif // CMDPROCESS_H
