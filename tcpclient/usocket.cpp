#include "usocket.h"
#ifdef linux
char oobByte;
volatile  char oobReceived = 0;
int * sockPtr;
void sig_urg(int signo)
{
     recv(*sockPtr, &oobByte, 1, MSG_OOB);
     oobReceived = 1;
 }
#endif
uSocket::uSocket()
{
    #ifdef WIN32
    WSADATA lp;
    WSAStartup(MAKEWORD( 2, 2 ),&lp);
    #endif
    s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);


    cin >> address;
    #ifdef WIN32
    hostEnt = gethostbyname(address.c_str());
    #endif
    cin.ignore();
    if(!hostEnt)
       {
           printf("Unable to collect gethostbyname\n");
           #ifdef WIN32
           WSACleanup();
           #endif
           return;
       }


        serverInfo.sin_family = PF_INET;
        #ifdef WIN32
        serverInfo.sin_addr = *((LPIN_ADDR)*hostEnt->h_addr_list);
        #elif linux
        serverInfo.sin_addr.s_addr = inet_addr(address.c_str());
        timeout.tv_sec = CLIENT_TIMEOUT/1000;
        timeout.tv_usec = 0;
        #endif
        serverInfo.sin_port = htons(10001);

        int retVal=connect(s,(LPSOCKADDR)&serverInfo, sizeof(serverInfo));
        if(retVal==SOCKET_ERROR)
        {
            printf("Unable to connect\n");
            #ifdef WIN32
            WSACleanup();
            #endif
            return;
        }

        setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
#ifdef linux
        timeout.tv_sec = 10;
        setsockopt(s,SOL_SOCKET,SO_SNDTIMEO,(const char*)&timeout,sizeof(timeout));
        signal(SIGURG, sig_urg);
        sockPtr = &s;
        fcntl(s,F_SETOWN,getpid());
#endif
    cout<<"CONNECTED!\n"<<endl;
}


int uSocket::recvBytes(char * buf,int bytes){
#ifdef WIN32
    timeout = 10000;
#elif linux
    timeout.tv_sec = 10;
#endif
    setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
    int ret = recv(s,buf,bytes,0);
#ifdef WIN32
    timeout = CLIENT_TIMEOUT;
#elif linux
    timeout.tv_sec = CLIENT_TIMEOUT/1000;
#endif
    setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
    return ret;

}
void uSocket::recvCmd(char * buf){
    buf--;
    do{
        buf++;
        if(recv(s,buf,1,0) == WSAENOTCONN){
            cout << "disconnected" <<endl;
            return;
        }
    } while((*buf)!='\0');
}
int uSocket::sendBytes(char * buf,int bytes){
        return send(s,buf,bytes,0);
}
void uSocket::reconnectSock(){
    #ifdef WIN32
    closesocket(s);
    #elif linux
    close(s);
    #endif
    char retry;
    s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    do {
    int retVal=connect(s,(LPSOCKADDR)&serverInfo, sizeof(serverInfo));
    setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char*)&timeout,sizeof(timeout));
#ifdef linux
        timeout.tv_sec = 10;
        setsockopt(s,SOL_SOCKET,SO_SNDTIMEO,(const char*)&timeout,sizeof(timeout));
#endif
    if(retVal==SOCKET_ERROR)
    {
        cout << "unable to recconect, retry?"<<endl;
        retry = cin.get();
        cin.ignore();
    } else {
        return;
    }
    } while (retry == 'y' || retry == 'y');
}
int uSocket::sendOOB(char a)
{
    return send(s,&a,1,MSG_OOB);
}

bool uSocket::recbOOB(char &a)
{
#ifdef linux
    if(!oobReceived) return false;
    oobReceived = 0;
    a = oobByte;
    return true;
#else
    unsigned long nonBlock = 1;
    unsigned long Block = 0;
    ioctlsocket(s,FIONBIO,&nonBlock);
    if (recv(s,&a,1,MSG_OOB) <0){
        ioctlsocket(s,FIONBIO,&Block);
        return false;
    }
    ioctlsocket(s,FIONBIO,&Block);
    return true;
#endif
}
uSocket::~uSocket(){
    #ifdef WIN32
    closesocket(s);
    WSACleanup();
    #elif linux
    close(s);
    #endif
}
