#ifndef SOCKET_H
#define SOCKET_H
#ifdef WIN32
#include <winsock2.h>


#elif linux
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cstring>
#include <signal.h>
#define DWORD unsigned
#define SOCKET_ERROR -1
#define INVALID_SOCKET -1
#define SOCKADDR_IN struct sockaddr_in
#define LPSOCKADDR sockaddr *
#endif
#define CLIENT_TIMEOUT 150000
using namespace std;
#include <iostream>
class uSocket
{
private:
#ifdef WIN32
    SOCKET s;
    SOCKET clientSock;
    DWORD timeout = CLIENT_TIMEOUT;

#elif linux
    int s;
    int clientSock;
    struct  timeval timeout;
#endif
private:
    void uSleep(int);
public:
    uSocket();
    void acceptConn();
    int recvBytes(char * buf, int bytes);
    int sendBytes(char * buf, int bytes);
    int sendOOB(char a);
    bool recbOOB(char &a);
    void recvCmd(char * buf);
    void disconnectSock();
   ~uSocket();
};

#endif // SOCKET_H
