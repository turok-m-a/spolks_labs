#ifndef SOCKET_H
#define SOCKET_H
#ifdef WIN32
#include <winsock2.h>
#elif linux
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cstring>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/fcntl.h>
#define DWORD unsigned
#define SOCKET_ERROR -1
#define INVALID_SOCKET -1
#define SOCKADDR_IN struct sockaddr_in
#define LPHOSTENT struct hostent *
#define LPSOCKADDR sockaddr *
#define WSAENOTCONN 10057
#endif
#define CLIENT_TIMEOUT 150000
#include <iostream>
using namespace std;
class uSocket
{
private:
#ifdef WIN32
    SOCKET s;
#elif linux
    int s;
#endif
    SOCKADDR_IN serverInfo;
#ifdef WIN32
    DWORD timeout = CLIENT_TIMEOUT;
#elif linux
    struct timeval timeout;
#endif
    string address;
    LPHOSTENT hostEnt;



public:
    uSocket();
    int recvBytes(char * buf, int bytes);
    int sendBytes(char * buf, int bytes);
    void recvCmd(char * buf);
    void reconnectSock();
    int sendOOB(char a);
    bool recbOOB(char &a);
    bool clientConnected= true;
   ~uSocket();
};

#endif // SOCKET_H
