#include "cmdprocess.h"

cmdProcess::cmdProcess( uSocket *s)
{
    sock = s;
}
void cmdProcess::doProcessing(char * cmd){
      sock->sendBytes(cmd,strlen(cmd)+1);
      if(strstr(cmd,"put")){
          putF(cmd);
          return;
      }
      if (strstr(cmd,"get")){
          getF(cmd);
          return;
      }
      if (strstr(cmd,"bye")){
          sock->clientConnected = false;
          return;
      }
       char response[100];
       sock->recvCmd(response);
       cout << response <<endl;
       return;
}
void cmdProcess::getF(char * cmd){
    char response[14];
    sock->recvBytes(response,14);
    long sz= atoi(response);
    if (!sz){
        cout << "file not found or empty" << endl;
        return;
    }
    sock->sendBytes((char*)&filePos,sizeof(long long)); //с какого байта хотим выкачать файл?
    FILE * f;
    if (filePos){
     f = fopen(cmd+4,"rb+"); //докачиваем
    } else {
     f = fopen(cmd+4,"wb");
    }
    fseek(f,filePos,SEEK_SET);
    char  buf[BLOCK];
    int bs = BLOCK;
    sz-=filePos;
    cout << "sz "<< sz <<endl;
    unsigned s = 0;
            while (sz>0) {
                if (sz < BLOCK){
                    bs = sz;
                }
                //Sleep(100);
                  int status = sock->recvBytes(buf,bs);
                if(			status ==  SOCKET_ERROR ){
                    cout<<"DISCONNECTED"<<endl;
                    filePos = ftell(f);   //какой байт в файл записан последним
                    fclose(f);
                    sock->reconnectSock();
                    return;
                }
                fwrite(buf,status,1,f);
                sz-=status;
                s++;
                if (!(s%1000)){
                cout << sz << endl; //прогресс
                char a;
                if (sock->recbOOB(a)) cout << "! O O B !" << a;
                }
            }
      fclose(f);
      filePos = 0;
}

void cmdProcess::putF(char * cmd){
    FILE * f = fopen(cmd+4,"rb");
    if (f == NULL){
        sock->sendBytes(emptyFileSize,14);
        return;
    }
    fseek(f, 0L, SEEK_END);
    long sz = ftell(f);
    char asciiSize[14];
    char buf[BLOCK];
    char * tmp =(char*) to_string(sz).c_str();
    strcpy(asciiSize,tmp);
    sock->sendBytes(asciiSize,14);
    sock->recvBytes((char*)&filePos,sizeof(long long));
    sz-=filePos;
    int bs = BLOCK;
    rewind(f);
    fseek(f,filePos,SEEK_CUR);
    unsigned s = 0;
    while (sz>0) {
        if (sz < BLOCK){
            bs = sz;
        }
        fread(buf,bs,1,f);
        if(sock->sendBytes(buf,bs) <= 0){
            cout<<"DISCONNECTED"<<endl;
            sock->reconnectSock();
            return;
        }
        sz-=bs;
        s++;
        if (!(s%1000)){
        cout << sz << endl; //прогресс
        }
    }
    fclose(f);
    filePos = 0;
}
