#include <cmdprocess.h>
#include <usocket.h>
int main(int argc, char *argv[])
{
    uSocket a;
    cmdProcess proc(&a);
    bool clientConnected = 1;
    char command[100];
    command[0]='\n';
    while(true){
    a.acceptConn();
    while(clientConnected){
        a.recvCmd(command);
        proc.doProcessing(command);
    }
    }
    return 0;
}

