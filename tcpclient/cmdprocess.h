#ifndef CMDPROCESS_H
#define CMDPROCESS_H
#include <usocket.h>
#include <time.h>
#include <iostream>
#define BLOCK 10000
class cmdProcess
{
private:
    void getF(char *);
    void putF(char *);
    uSocket * sock;
    char emptyFileSize[14] = "0";
public:
    cmdProcess(uSocket *);
    void doProcessing(char *);
    long long filePos = 0;
};

#endif // CMDPROCESS_H
