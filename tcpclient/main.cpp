#include <cmdprocess.h>
#include <usocket.h>
#include <iostream>
int main(int argc, char *argv[])
{
    uSocket a;
    cmdProcess proc(&a);
    bool clientConnected = 1;
    char command[100];
    command[0]='\n';
    while(a.clientConnected){
        gets(command);
        proc.doProcessing(command);
    }
    return 0;
}
